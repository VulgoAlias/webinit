#!/bin/bash
NAMEOK=0
while [ $NAMEOK -ne 1 ]
do
    echo -e "[\e[96m...\e[0m] Please insert \e[36mweb name\e[0m:"
    read WEBNAME
    if [ ! -z $WEBNAME ]
    then
        VHOSTPATH="/etc/apache2/sites-available/$WEBNAME.conf"
        if [ ! -e $VHOSTPATH ]
        then
            NAMEOK=1
        else
            CONTB=0
            while [ $CONTB -ne 1 ]
            do
                echo -e "[\e[33mWARN\e[0m] Name \e[35m$WEBNAME\e[0m already in use, do you want to continue? (\e[90my/n\e[0m)"
                read CONTINUE
                if [ ! -z $CONTINUE ]
                then
                    if [ $CONTINUE = "y" ]
                    then
                        NAMEOK=1
                        CONTB=1
                    else
                        if [ $CONTINUE = "n" ]
                        then
                            echo -e "[\e[31mERROR\e[0m] Script aborted by user."
                            exit 0
                        fi
                    fi
                fi
            done
        fi
    else
        echo -e "[\e[33mWARN\e[0m] Web name cannot be empty."
    fi
done
echo -e "[\e[32mOK\e[0m] Web name \e[35m$WEBNAME\e[0m set successfully."

DIRNAME="$WEBNAME"
echo -e "[\e[96m...\e[0m] Please insert \e[36mproject folder name\e[0m (\e[90m$WEBNAME\e[0m):"
read _DIRNAME
if [ ! -z "$_DIRNAME" ]
then
    if [ $_DIRNAME != "" ]
    then
        DIRNAME="$_DIRNAME"
    fi
fi
echo -e "[\e[32mOK\e[0m] Project folder name is \e[35m$DIRNAME\e[0m"

PATHOK=0
while [ $PATHOK -ne 1 ]
do
    WEBPATH="/var/www/html/"
    echo -e "[\e[96m...\e[0m] Please insert path to \e[36mproject folder\e[0m: (\e[90m$WEBPATH\e[0m):"
    read _WEBPATH
    if [ ! -z "$_WEBPATH" ]
    then
        if [ $_WEBPATH != "" ]
        then
            WEBPATH="$_WEBPATH"
        fi
    fi

    if [ -d $WEBPATH ]
    then
        echo -e "[\e[32mOK\e[0m] Path OK."
        PATHOK=1
    else
        echo -e "[\e[31mERROR\e[0m] The path \e[90m$WEBPATH\e[0m is not a valid directory."
    fi
done

echo -e "[\e[32mOK\e[0m] Path to project folder is: \e[35m$WEBPATH\e[0m"

URL="$WEBNAME.loc"
echo -e "[\e[96m...\e[0m] Please insert \e[36mlocal URL\e[0m (\e[90m$URL\e[0m): "
read _URL
if [ ! -z "$_URL" ]
then
    if [ $_URL != "" ]
    then
        URL="$_URL"
    fi
fi
echo -e "[\e[32mOK\e[0m] URL is \e[35m$URL\e[0m"

EMPTY=1
INDEXPATH=""
echo -e "[\e[96m...\e[0m] Please insert \e[35mpath to index.php\e[0m relative from project folder (\e[90m$INDEXPATH\e[0m):"
read _INDEXPATH
if [ ! -z "$_INDEXPATH" ]
then
    if [ $_INDEXPATH != "" ]
    then
        INDEXPATH="$_INDEXPATH"
        EMPTY=0
    fi
fi
echo -e "[\e[32mOK\e[0m] Path to index.php is \e[35m$INDEXPATH\e[0m."

echo -e "[\e[96m...\e[0m] Creating a virtual host."
VHOSTPATH="/etc/apache2/sites-available/$WEBNAME.conf"
if [ -d "/etc/apache2/sites-available" ]
then
    if [ ! -e $VHOSTPATH ]
    then
        VHOSTCREATED=$(sudo touch $VHOSTPATH)
        if [$? -ne 0]
        then
            echo -e "[\e[31mERROR\e[0m] Virtual host file could not be created, exitting now."
            exit 2
        else
            echo -e "[\e[32mOK\e[0m] Virtual host file successfully created."
        fi
    else
        echo -e "[\e[33mWARN\e[0m] Virtual host already exists."
    fi
else
    echo -e "[\e[31mERROR\e[0m] Cannot find 'sites-available' Apache folder, exiting now."
    exit 1
fi

echo -e "[\e[96m...\e[0m] Updating virtual host permissions."
`sudo chmod 755 $VHOSTPATH`
if [ $? -eq 0 ]
then
    echo -e "[\e[32mOK\e[0m] Virtual host permissions updated."
else
    echo -e "[\e[31mERROR\e[0m] Unable to update virtual host permissions."
fi

echo -e "[\e[96m...\e[0m] Configuring virtual host."
DOCROOT="$WEBPATH$DIRNAME/"
if [ $EMPTY -eq 0 ];
then
    DOCROOT="$DOCROOT$INDEXPATH/"
fi
VHOST="<VirtualHost *:80>\n\tServerAdmin webmaster@localhost\n\tDocumentRoot $DOCROOT\n\tServerName $URL\n\tRewriteEngine on\n\tRewriteCond %%{REQUEST_FILENAME} !-f\n\tRewriteCond %%{REQUEST_FILENAME} !-d\n\tRewriteRule ^(.*)\$ /index.php?path=\$1 [NC,L,QSA]\n\tErrorLog \${APACHE_LOG_DIR}/error.log\n\tCustomLog \${APACHE_LOG_DIR}/access.log combined\n</VirtualHost>\n"
`sudo bash -c "printf \"$VHOST\" > $VHOSTPATH"`
if [ $? -eq 0 ]
then
    echo -e "[\e[32mOK\e[0m] Virtual host configured successfully."
else
    echo -e "[\e[31mERROR\e[0m] Unable to configure virtual host."
fi

echo -e "[\e[96m...\e[0m] Enabling virtual host."
VHAUTH=$(a2ensite $WEBNAME)
#if [ $? -eq 0 ]
#then
#    echo -e "[\e[32mOK\e[0m] Virtual host \e[35m$WEBNAME\e[0m enabled."
#else
#    echo -e "[\e[31mERROR\e[0m] Unable to enable virtual host \e[35m$WEBNAME\e[0m."
#fi

echo -e "[\e[96m...\e[0m] Reloading Apache"
`sudo service apache2 reload`
if [ $? -eq 0 ]
then
    echo -e "[\e[32mOK\e[0m] Apache reloaded successfully."
else
    echo -e "[\e[31mERROR\e[0m] Unable to reload Apache."
fi

echo -e "[\e[96m...\e[0m] Creating virtual host symlink"
SYMPATH="$WEBPATH$DIRNAME/virtual_host.conf"
if [ -s $SYMPATH ]
then
    echo -e "[\e[33mWARN\e[0m] Symlink already exists."
else
    `ln -s $VHOSTPATH $SYMPATH`
    if [ $? -eq 0 ]
    then
        echo -e "[\e[32mOK\e[0m] Symlink created."
    else
        echo -e "[\e[31mERROR\e[0m] Unable to create symlink."
    fi
fi
echo -e "[\e[96m...\e[0m] Checking hosts."
HOSTREC=$(printf "127.0.0.1\t$URL")
HOSTURL=$(cat "/etc/hosts" | grep -c "$URL")
if [ $HOSTURL -eq 0 ]
then
    echo -e "[\e[96m...\e[0m] Creating new record in /etc/hosts ($URL at 127.0.0.1)"
    sudo bash -c "echo \"$HOSTREC\" >> /etc/hosts"
    if [ $? -eq 0 ]
    then
        echo -e "[\e[32mOK\e[0m] Created new record in /etc/hosts."
    else
        echo -e "[\e[31mERROR\e[0m] Unable to create new record in /etc/hosts."
    fi
else
    echo -e "[\e[33mWARN\e[0m] Record in /etc/hosts already exists."
fi
