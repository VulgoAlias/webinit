Webinit is a simple shell script, that eases and simplifies the process of setting up a new webiste on Ubuntu with Apache2 installed.

It does several things:
1) Creates a virtual host in /etc/apache2/sites-available and enables it.
2) Creates a record in /etc/hosts
3) Creates a soft symlink to virtual host in project folder.

Basic usage:
Run the script. The script is interactive and currently does not accept any parameters.

Please note that this is the first alpha release, so both functionality and testing done is limited.